Python Notes 2

Datetime
    from datetime import datetime

    datetime.now()
    Return the current date and time
        now = datetime.now()

        print(now)
        print(now.year)
        print(now.month)
        print(now.day)

        print '%02d-%02d-%04d' % (now.month, now.day, now.year)
        print '%02d:%02d:%02d' % (now.hour, now.minute, now.second)

Comparators

    Equal to (==)
        2 == 2
           True
        2 == 5
            False

    Not equal to (!=)
        2 != 5
            True
        2 != 2
            False
    
    Less than (<)
        2 < 5
            True
        5 < 2
            False
    
    Less than or equal to (<=)
        2 <= 2
            True
        5 <= 2
            False
    
    Reverse for greater than

    and - returns true when boths sides are true
        False and False #false
        True and True #True
        -(-(-(-2))) == -2 and 4 >= 16 ** 0.5 #True
    
    or - returns true with at least one side is true
        1 < 2 or 2 > 3 #True
        1 > 2 or 2 > 3 #False

    not - returns true for false statements and false for true
        not False # True
        not 41 > 40 # False
    
    Order of Comparators
        not, and, or

Conditional Syntax

    def greater_less_equal_5(answer):
        if answer > 5:
            return 1
        elif answer < 5:          
            return -1
        else:
            return 0