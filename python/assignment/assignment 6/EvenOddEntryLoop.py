integer = int(input("Please input an integer (999 to stop) : "))

while (integer != 999):
    if integer % 2 == 0:
        print("Good job!")
        integer = int(input("Please input an integer (999 to stop) : "))
    elif not integer % 2 == 0:          
        print("Error - Isn't an even number, retrying.")
        integer = int(input("Please input an integer (999 to stop) : "))
    else:
        print("That isn't a valid input!")

