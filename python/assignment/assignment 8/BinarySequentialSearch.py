integers = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]
print(integers)

userinput = input("Find position of integer - ")
search = integers.index(int(userinput))

print("Integer " + str(userinput) + " is located at position " + str(search))