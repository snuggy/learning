class FunctionExample {
 
    public static void main(String[] args) {
    // set int variables
    int a = 11;
    int b = 6;
    // send to print function
    PrintFunction(a, b);
    }
 
    public static int PrintFunction(int n1, int n2) {
        // print the string using a the string quotes to create a space between the ints
        System.out.println(n1+" "+n2);
        return (0);
    }
}