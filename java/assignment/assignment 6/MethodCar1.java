public class MethodCar1 extends Car1 {
    public static void main (String args []) {
        // Car1 reference and object
    Car1 obj1 = new Car1(); // Car1 reference but DerivedClass object
    Car2 obj2 = new Car2(); // Calls the method from Car1 class
    Car3 obj3 = new Car3(); // Calls the method from Car1 class
    
    obj1.VehicleType(10); //Calls VehicleType from Car1
    obj2.VehicleType(10, 20); //Calls VehicleType from Car2
    obj3.VehicleType("Hello World!!"); //Calls VehicleType from Car3
    }
}