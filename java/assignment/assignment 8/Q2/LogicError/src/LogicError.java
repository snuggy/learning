/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fraser
 */
import java.util.Scanner; //import scanner class
public class LogicError {
/**
* @param args the command line arguments
* Following Program is not printing the right statement.
*
*/
public static void main(String[] args) {
// TODO code application logic here
Scanner sc=new Scanner(System.in); //Allows us to read a number from System.in
System.out.println("Enter your number!!"); //print instructions
int num = sc.nextInt();//get user response
if (num >= 50) //check if number is greater than 50 and print a statement if boolean is true
{
System.out.println("Your input is greater than 50!!!");
}
else //else, the number is less, so print the following statement
{
System.out.println("Your input is less than 50!!!");
}
}
}