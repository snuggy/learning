class Demo1 {
        public Demo1() {
    //create a new constructor
 System.out.println("This is my first constructor");
    }
 public static void main(String args[]) {
     //call the demo1 constructor
        new Demo1();
    }
}

/*
1) Create a non-arg constructor that prints a string.
2) Call the constructor
*/