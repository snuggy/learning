/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fraser.patterson
 */
class StudentClass
{
 int rollNum;
 String studentName;
 Address studentAddr;
 StudentClass(int roll, String name, Address addr){
 this.rollNum=roll;
 this.studentName=name;
 this.studentAddr = addr;
 }
 public static void main(String args[]){
//Create a new list that mirrors Address.java and call it .ad
// Then populate it with the following values
 Address ad = new Address(80, "Mount Tarcoola", "WA", "Australia");
 
//Create a new StudentClass with the following values
//Populate it with some values, and the ad class we defined earlier
 StudentClass obj = new StudentClass(123, "Hashir", ad);
 
 System.out.println(obj.rollNum); //Call RollNum from StudentClass .obj
 System.out.println(obj.studentName); //Call StudentName from our StudentClass .obj
 System.out.println(obj.studentAddr.streetNum); //Call streetNum from our StudentClass .obj (refences .ad, which stores all of the values from address)
 System.out.println(obj.studentAddr.city); //Call city from our StudentClass .obj (refences .ad, which stores all of the values from address)
 System.out.println(obj.studentAddr.state); //Call state from our StudentClass .obj (refences .ad, which stores all of the values from address)
 System.out.println(obj.studentAddr.country); //Call country from our StudentClass .obj (refences .ad, which stores all of the values from address)
/*
The reason we can call Address variables from the studentAddr class is because
we added them by listing them with the StudentClass. This allows us to print
them out.
*/
 }
}

