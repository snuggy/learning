import java.io.File;
import java.io.IOException;
public class FileDemo1 {
    public static void main( String[] args ) {
    try {
        //Declare the file variable
        File file = new File("FileDemo.txt");

//call .createNewFIle from the file import
 boolean fvar = file.createNewFile();
 //run this code if it succeeds in creating the file
    if (fvar) {
        System.out.println("File has been created successfully");
        }
//run this code if it fails (boolean returns false, file exists)
    else {
        System.out.println("File already present at the specified location");
    }
 }
 //if the code throws an exception, print the stack trace for debugging
    catch (IOException e) {
        System.out.println("Exception Occurred:");
            e.printStackTrace();
        }
    }
}

 /*
 1) createNewFile attempts to create a file.
 2) If the file is created successfully, a boolean returns true
 3) If the file isn't created successfully (already exists), it returns false
 4) If java doesn't have permission or any other errors, throw an exception.
 */