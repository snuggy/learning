public class UseAnimals
{
 public static void main(String[] args)
 {
//create new objects and give them names
 Dog myDog = new Dog();
 Cow myCow = new Cow();
 Snake mySnake = new Snake();

 //call the variables on the object instances
 myDog.setName("My dog Murphy");
 myCow.setName("My cow Elsie");
 mySnake.setName("My snake Sammy");

 //print the name of the animal and the word " says ", followed by calling the speak class on the object
 System.out.print(myDog.getName() + " says ");
 myDog.speak();

 System.out.print(myCow.getName() + " says ");
 myCow.speak();
 
 System.out.print(mySnake.getName() + " says ");
 mySnake.speak();
 }
}

/* Code Explained

1) Three different animal objects are created 
2) Call the .setName functions on the objects and give them a string
3) Print the .getName function (that reads the .setName we did earlier), print a string and call the .speak function
*/