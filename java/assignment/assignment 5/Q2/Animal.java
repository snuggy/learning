public abstract class Animal
{
 private String name;
 public abstract void speak();
 //getName returns the name variable
 public String getName()
 {
 return name;
 }
 public void setName(String animalName)
 {
     //make animalname = name for getname to return it
 name = animalName;
 }
}
