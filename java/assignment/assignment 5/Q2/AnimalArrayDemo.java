public class AnimalArrayDemo
{
 public static void main(String[] args)
 {
     //create an array to read out the variables
 Animal[] animalRef = new Animal[3];
 animalRef[0] = new Dog();
 animalRef[1] = new Cow();
 animalRef[2] = new Snake();
 //execute code while the array has objects in it
 for(int x = 0; x < animalRef.length; ++x)
 animalRef[x].speak();
 }
}

/* Code Explained

1) A new animal array is created called animalRef, it is composed of three Animal objects.
2) The array is created with three objects and each is given an object to run.
3) For loop runs as long as the array length > x (the current array being processed)
4) Prints out the current array.
*/