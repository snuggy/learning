class Suzuki extends Car1 {
    //call to print public suzuki
    public Suzuki(){
        System.out.println("Public Suzuki");
    }
    //call to print suzuki brand
    public void brand() {
        System.out.println("Suzuki Brand");
    }
    //call to print suzuki speed
    public void speed() {
        System.out.println("Suzuki Speed");
    }
    public static void main(String[] args) {
    }
}