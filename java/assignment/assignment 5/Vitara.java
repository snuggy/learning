class Vitara extends Suzuki{
    //no arg constructor for vitara
    public Vitara() {
        System.out.println("Vitara Non Arg");
    }
//call to print speed from the vitara class
    public void speed() {
        System.out.println("Vitara Speed");
    }
    
    public static void main(String[] args){
        //create a new vitara object named vehicle
        Vitara vehicle = new Vitara();
        //call methods on vehicle to print things out
        vehicle.printVehicleType();
        vehicle.brand();
        vehicle.speed();
    }
}

/*
Output Explained

The three classes connect together using "extends" to create a class heirachy. The vitara class calls the suzuki class, which calls the car1 class, this means that the vitara class is able to call Car1 classes through the suzuki class.
The last line is a doubled up class, because the suzuki class also has a class with the same name. The vitara prioritises it's own class.

*/