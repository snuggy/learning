class Car1 {
    //Call to display Class Car1
    public Car1() {
        System.out.println("Class Car1");
    }

    //Call to display Vehicle Type Car1
    public void printVehicleType() {
        System.out.println("Vehicle Type: Car1");
    }

    public static void main(String[] args) {
    }
}